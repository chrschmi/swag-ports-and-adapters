package de.osp.swag.paa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortsDemoLiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(PortsDemoLiveApplication.class, args);
	}

}
