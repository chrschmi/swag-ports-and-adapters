package de.osp.swag.paa.domain.port;

import de.osp.swag.paa.domain.model.BusinessModel;

public interface PrimaryPort {

	Iterable<BusinessModel> retrieveAllThings();
	
	boolean save(BusinessModel thing); 
	
}
