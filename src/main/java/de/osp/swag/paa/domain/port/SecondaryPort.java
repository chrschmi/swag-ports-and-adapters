package de.osp.swag.paa.domain.port;

import java.util.Collection;

import de.osp.swag.paa.domain.model.BusinessModel;

public interface SecondaryPort {

	Collection<BusinessModel> fetchAll();
	
	boolean save(BusinessModel model);
	
}
