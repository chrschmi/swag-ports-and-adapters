package de.osp.swag.paa.domain.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.UUID;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import de.osp.swag.paa.domain.model.BusinessModel;
import de.osp.swag.paa.domain.port.PrimaryPort;
import de.osp.swag.paa.domain.port.SecondaryPort;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class BusinessLogicService implements PrimaryPort {

	private final SecondaryPort db;
	
	@Override
	public Iterable<BusinessModel> retrieveAllThings() {
		
		System.out.println("Was asked to retrieve all the things.");
		
		return db.fetchAll();
	}

	@Override
	public boolean save(BusinessModel thing) {
		assertThat(thing).isNotNull();
		
		System.out.println("Was asked to save a thing with id "+ thing.getId());
		
		return db.save(thing);
	}

	@EventListener(ApplicationReadyEvent.class)
	private void initData() {
		save(new BusinessModel(UUID.randomUUID().toString(), Instant.now(), "Bootstrap Data 1, yay."));
		save(new BusinessModel(UUID.randomUUID().toString(), Instant.now(), "Bootstrap Data 2, nay."));
		save(new BusinessModel(UUID.randomUUID().toString(), Instant.now(), "Bootstrap Data 3, ehh."));
		save(new BusinessModel(UUID.randomUUID().toString(), Instant.now(), "Bootstrap Data 4, meh."));
	}
	
}
