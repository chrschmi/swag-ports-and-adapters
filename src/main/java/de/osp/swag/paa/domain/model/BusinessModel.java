package de.osp.swag.paa.domain.model;

import java.time.Instant;

import lombok.Value;

@Value
public class BusinessModel {

	private String id;
	private Instant creationDate;
	private String description;
	
}
