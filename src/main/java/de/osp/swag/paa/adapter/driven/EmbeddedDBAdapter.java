package de.osp.swag.paa.adapter.driven;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import de.osp.swag.paa.domain.model.BusinessModel;
import de.osp.swag.paa.domain.port.SecondaryPort;

@Repository
public class EmbeddedDBAdapter implements SecondaryPort {

	private final Map<String, BusinessModel> repo = new HashMap<>();
	
	@Override
	public Collection<BusinessModel> fetchAll() {
		return repo.values();
	}

	@Override
	public boolean save(BusinessModel model) {
		return null == repo.put(model.getId(), model);
	}
	
	

}
