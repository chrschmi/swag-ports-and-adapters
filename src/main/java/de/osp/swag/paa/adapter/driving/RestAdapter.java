package de.osp.swag.paa.adapter.driving;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.osp.swag.paa.domain.model.BusinessModel;
import de.osp.swag.paa.domain.port.PrimaryPort;
import lombok.AllArgsConstructor;

@RequestMapping("/things")
@RestController
@AllArgsConstructor
public class RestAdapter {

	private final PrimaryPort service;
	
	@CrossOrigin
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public Iterable<BusinessModel> getThings() {
		return service.retrieveAllThings();
	}
	
}
